<?php

namespace App\Services;

use Podio;

class Recommendations
{
    protected $clientId;

    protected $clientSecret;

    protected $podio;

    public function __construct()
    {
        $this->clientId     = config('podio.keys.system.client_id');
        $this->clientSecret = config('podio.keys.system.client_secret');

        Podio::setup($this->clientId, $this->clientSecret);
    }
}