<?php

namespace App\Services;

use Podio;
use PodioApp;
use PodioItem;
use PodioTextItemField;
use PodioCategoryItemField;
use Illuminate\Http\Request;
use PodioItemFieldCollection;
use App\Services\Recommendations;
use Illuminate\Support\Facades\Log;

class ProtectinsureRecommendations extends Recommendations
{
    /**
     * Recommendations app id
     *
     * @var string
     */
    protected $id;

    /**
     * Recommendations app token
     *
     * @var string
     */
    protected $token;

    /**
     * Construct the object
     */
    public function __construct()
    {
        parent::__construct();

        $this->id        = config('podio.keys.protectinsure.app.recommendations.id');
        $this->token    = config('podio.keys.protectinsure.app.recommendations.token');

        Podio::authenticate_with_app($this->id, $this->token);
    }

    public function handle(Request $request)
    {
        try {
            $this->sendToPodio($request);
        } catch(\Exception $e) {
            Log::info($e);
            return false;
        }

        return true;
    }

    protected function sendToPodio(Request $request)
    {
        Log::info($request['quotedata']['quoteResponse']['clients'][0]['portfolios']);

        foreach($request['quotedata']['quoteResponse']['clients'][0]['portfolios'] as $portfolio) {
            foreach($portfolio['products'] as $product) {

                $item = new PodioItem(array(
                'app' => new PodioApp($this->id),
                'fields' => new PodioItemFieldCollection()
                ));

                $item->fields['unique-id'] = new PodioTextItemField();
                $item->fields['unique-id']->values = $request['quotedata']['quoteResponse']['referenceId'];

                $item->fields['coverage-name'] = new PodioTextItemField();
                $item->fields['coverage-name']->values = $portfolio['supplier']['name'];

                $item->fields['life-insurer-portfolio-name'] = new PodioTextItemField();
                $item->fields['life-insurer-portfolio-name']->values = $portfolio['name'];


                $item->fields['coverage-type'] = new PodioCategoryItemField();

                switch($product['name']){
                    case 'Life':
                        $item->fields['coverage-type']->values = 1;
                        break;

                     case 'Life Cover':
                        $item->fields['coverage-type']->values = 1;
                        break;

                    case 'Total And Permanent Disability':
                        $item->fields['coverage-type']->values = 7;
                        break;

                    case 'TPD Cover':
                        $item->fields['coverage-type']->values = 7;
                        break;

                    case 'Trauma':
                        $item->fields['coverage-type']->values = 6;
                        break;

                    case 'Income Protection':
                        $item->fields['coverage-type']->values = 2;
                        break;

                    case 'Income Protection - Extended Indemnity - with Advantage option':
                        $item->fields['coverage-type']->values = 2;
                        break;

                    case 'Income Protection Cover':
                        $item->fields['coverage-type']->values = 2;
                        break;
                }

                $resolvedNeeds = array_values($product['resolvedNeeds'][0])[0];

                $item->fields['sum-insured-2'] = new PodioTextItemField();
                $item->fields['sum-insured-2']->values = array_key_exists('sumInsured', $resolvedNeeds) ?
                    (string) $resolvedNeeds['sumInsured'] : (string) $resolvedNeeds['monthlyBenefit'];

                $item->fields['options'] = new PodioTextItemField();
                $item->fields['options']->values = implode(", ",
                    array_map(function ($key, $value) {
                        if (!is_array($value)) {
                            return ucfirst($key) . ': ' . $value;
                        }
                    },
                    array_keys(array_values($product['resolvedNeeds'][0])[0]),
                    array_values($product['resolvedNeeds'][0])[0])
                );

                $item->fields['premium-2'] = new PodioTextItemField();
                $item->fields['premium-2']->values = (string) $portfolio['premiumTotal']['premium']['M'];

                $item->fields['frequency-2'] = new PodioTextItemField();
                $item->fields['frequency-2']->values = 'M';

                $item->fields['policy-fee'] = new PodioTextItemField();
                $item->fields['policy-fee']->values = (string) $portfolio['policyFeeTotal']['premium']['M'];

                $item->save();

            }
        }

    }
}