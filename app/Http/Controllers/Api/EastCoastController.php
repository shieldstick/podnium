<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\EastCoastRecommendations;

class EastCoastController extends Controller
{
    /**
     * Client specific service object
     *
     * @var App\Services\EastCoastRecommendations
     */
    protected $service;

    /**
     * Controller constructor
     *
     */
    public function __construct()
    {
        $this->service = new EastCoastRecommendations;
    }

    /**
     * Store the data from omnium
     *
     * @param  Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->service->handle($request)) {
            return response()->json(['message' => 'Data successfully processed.'], 200);
        }

        return response()->json(['message' => 'Unprocessable entity.'], 422);
    }
}
