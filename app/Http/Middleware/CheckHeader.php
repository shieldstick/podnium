<?php

namespace App\Http\Middleware;

use Closure;

class CheckHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! $request->header('Authorization')) {
            return response()->json(['message' => 'Authorization header is not set.'], 401);
        }

        if($request->header('Authorization') != config('auth.guards.header.key')) {
            return response()->json(['message' => 'Invalid authorization header.'], 401);
        }

        return $next($request);
    }
}
