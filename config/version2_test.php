<?php

return array (
  'appid' => '12362043',
  'token' => '315a7319c0a24389950afe3f9796b82f',
  'cid' => 'jarickson',
  'ckey' => 'DVG6ofBTZlI5Yuae4m2yHsN7Lh3QRxi5LkRuo0qhDVGYq6UsRVXJR74LnAVP7hWd',
  'podioitemid' => NULL,
  'quotedata' =>
  array (
    'quoteRequest' =>
    array (
      'adviserCredentials' => NULL,
      'prepopulationIds' => NULL,
      'id' => 'e15a6979-65ca-4b29-809c-22b6424fadf0',
      'needsAnalysisId' => NULL,
      'needsAnalysis' => NULL,
      'referenceId' => 'OPP865',
      'clients' =>
      array (
        0 =>
        array (
          'firstName' => 'Scott',
          'lastName' => 'Gellatly',
          'dateOfBirth' => '1991-09-08T00:00:00',
          'age' => 26,
          'title' => NULL,
          'income' => 150000,
          'gender' => 'M',
          'smoker' => false,
          'occupationId' => '221111',
          'occupationDescription' => 'Accountant',
          'customOccupations' =>
          array (
          ),
          'state' => 'ACT',
          'needs' =>
          array (
            0 =>
            array (
              'TRM' =>
              array (
                'sumInsured' => 50000,
                'structure' => 'S',
                'premiumWaiver' => 'N',
                'owner' => 'O',
                'rollover' => 'N',
                'linkedNeeds' =>
                array (
                ),
                'productCodes' =>
                array (
                ),
              ),
            ),
            1 =>
            array (
              'TPS' =>
              array (
                'sumInsured' => 70000,
                'structure' => 'S',
                'premiumWaiver' => 'N',
                'occupationType' => 'E',
                'owner' => 'O',
                'rollover' => 'N',
                'productCodes' =>
                array (
                ),
              ),
            ),
          ),
          'loadings' =>
          array (
            'supplierOverrides' =>
            array (
            ),
            'percentage' =>
            array (
            ),
            'dollarsPerThousand' =>
            array (
            ),
          ),
        ),
      ),
      'settings' =>
      array (
        'includedProducts' =>
        array (
          0 => 'AIG',
          1 => 'AXA',
          2 => 'AML',
          3 => 'MPI',
          4 => 'SCS',
          5 => 'ANZ',
          6 => 'AES',
          7 => 'AST',
          8 => 'ASS',
          9 => 'AVS',
          10 => 'WBC',
          11 => 'BTS',
          12 => 'CBS',
          13 => 'CHS',
          14 => 'CLE',
          15 => 'COM',
          16 => 'CES',
          17 => 'EIS',
          18 => 'ENE',
          19 => 'RIO',
          20 => 'EQU',
          21 => 'FCE',
          22 => 'FCP',
          23 => 'GES',
          24 => 'IOE',
          25 => 'IOO',
          26 => 'KIN',
          27 => 'LGS',
          28 => 'MLC',
          29 => 'MES',
          30 => 'ING',
          31 => 'PSF',
          32 => 'PRI',
          33 => 'SBS',
          34 => 'SES',
          35 => 'SSA',
          36 => 'ACC',
          37 => 'TAS',
          38 => 'TLC',
          39 => 'TLP',
          40 => 'WAS',
          41 => 'ZUR',
          42 => 'ZUD',
        ),
        'excludedProducts' =>
        array (
          0 => 'ACS',
          1 => 'ASU',
          2 => 'CAR',
          3 => 'CAT',
          4 => 'CPS',
          5 => 'ESS',
          6 => 'FSS',
          7 => 'HES',
          8 => 'HOP',
          9 => 'INT',
          10 => 'LUC',
          11 => 'MTA',
          12 => 'MWE',
          13 => 'NGS',
          14 => 'QSU',
          15 => 'REI',
          16 => 'RES',
          17 => 'STA',
          18 => 'SSU',
          19 => 'TWU',
          20 => 'UNI',
          21 => 'VIC',
          22 => 'VIS',
          23 => 'MAA',
          24 => NULL,
          25 => 'ACS',
          26 => 'ASU',
          27 => 'CAR',
          28 => 'CAT',
          29 => 'CPS',
          30 => 'ESS',
          31 => 'FSS',
          32 => 'HES',
          33 => 'HOP',
          34 => 'INT',
          35 => 'LUC',
          36 => 'MTA',
          37 => 'MWE',
          38 => 'NGS',
          39 => 'QSU',
          40 => 'REI',
          41 => 'RES',
          42 => 'STA',
          43 => 'SSU',
          44 => 'TWU',
          45 => 'UNI',
          46 => 'VIC',
          47 => 'VIS',
          48 => 'MAA',
        ),
        'frequency' => 'Y',
        'superFrequency' => 'Y',
        'priceWeighting' => 0.0,
        'commissionOptions' =>
        array (
        ),
        'campaignOptions' =>
        array (
        ),
      ),
      'adviser' =>
      array (
        'email' => 'paul@jarickson.com.au',
        'crm' => 'PodioCRM',
      ),
    ),
    'quoteResponse' =>
    array (
      'id' => 'e15a6979-65ca-4b29-809c-22b6424fadf0',
      'referenceId' => 'OPP865',
      'clients' =>
      array (
        0 =>
        array (
          'quoteIdentifiers' =>
          array (
          ),
          'portfolios' =>
          array (
            0 =>
            array (
              'code' => 'CLEP1',
              'name' => 'Clearview LifeSolutions',
              'campaign' => NULL,
              'commission' => NULL,
              'supplier' =>
              array (
                'code' => 'CLE',
                'name' => 'ClearView',
                'type' => NULL,
                'fundType' => NULL,
                'logo' => 'static.omnilife.com.au/images/supplierlogo/100x57/CLE.png',
                'url' => NULL,
                'cmapRating' => NULL,
                'defaultCampaignCode' => NULL,
                'defaultCommissionCode' => NULL,
                'commissionText' => NULL,
                'products' => NULL,
                'commissionOptions' => NULL,
                'campaignOptions' => NULL,
              ),
              'products' =>
              array (
                0 =>
                array (
                  'code' => 'CLE1',
                  'name' => 'Life Cover',
                  'pdsName' => 'Life: Life Cover',
                  'commission' => NULL,
                  'score' =>
                  array (
                    'feature' =>
                    array (
                      'raw' => 91.0,
                      15 => 'A',
                      5 => 'A',
                      'star' => '****+',
                    ),
                    'combined' =>
                    array (
                      'raw' => 91.0,
                      15 => 'A',
                      5 => 'A',
                      'star' => '****+',
                    ),
                    'featurePerNeed' =>
                    array (
                      'TRM' =>
                      array (
                        'raw' => 97.14066422941671,
                        15 => 'A+',
                        5 => 'A',
                        'star' => '*****',
                      ),
                    ),
                    'combinedPerNeed' =>
                    array (
                      'TRM' =>
                      array (
                        'raw' => 97.0,
                        15 => 'A+',
                        5 => 'A',
                        'star' => '*****',
                      ),
                    ),
                  ),
                  'coveredNeeds' =>
                  array (
                    0 => 'TRM',
                    1 => 'FEE',
                  ),
                  'resolvedNeeds' =>
                  array (
                    0 =>
                    array (
                      'TRM' =>
                      array (
                        'sumInsured' => 50000,
                        'structure' => 'S',
                        'owner' => 'O',
                        'rollover' => 'N',
                        'premiumWaiver' => 'N',
                      ),
                    ),
                  ),
                ),
                1 =>
                array (
                  'code' => 'CLET',
                  'name' => 'TPD Cover',
                  'pdsName' => 'TPD standalone: TPD Cover',
                  'commission' => NULL,
                  'score' =>
                  array (
                    'feature' =>
                    array (
                      'raw' => 91.0,
                      15 => 'A',
                      5 => 'A',
                      'star' => '****+',
                    ),
                    'combined' =>
                    array (
                      'raw' => 91.0,
                      15 => 'A',
                      5 => 'A',
                      'star' => '****+',
                    ),
                    'featurePerNeed' =>
                    array (
                      'TPS' =>
                      array (
                        'raw' => 90.67743267048424,
                        15 => 'A',
                        5 => 'A',
                        'star' => '****+',
                      ),
                    ),
                    'combinedPerNeed' =>
                    array (
                      'TPS' =>
                      array (
                        'raw' => 91.0,
                        15 => 'A',
                        5 => 'A',
                        'star' => '****+',
                      ),
                    ),
                  ),
                  'coveredNeeds' =>
                  array (
                    0 => 'TPS',
                  ),
                  'resolvedNeeds' =>
                  array (
                    0 =>
                    array (
                      'TPS' =>
                      array (
                        'sumInsured' => 70000,
                        'structure' => 'S',
                        'occupationType' => 'O',
                        'owner' => 'O',
                        'premiumWaiver' => 'N',
                        'rollover' => 'N',
                      ),
                    ),
                  ),
                ),
              ),
              'occupation' =>
              array (
                'supplierCode' => 'CLE',
                'id' => '5',
                'description' => 'Accountant - CPA/CA qualified',
                'classTRM' => 'OR',
                'classTPDAny' => 'A',
                'classTPDOwn' => 'A',
                'classTPDADL' => 'A',
                'classTRA' => 'OR',
                'classINC' => 'AAA',
                'classBUS' => 'AAA',
                'supportsTRM' => true,
                'supportsTPDAny' => true,
                'supportsTPDOwn' => true,
                'supportsTPDADL' => true,
                'supportsTRA' => true,
                'supportsINC' => true,
                'supportsBUS' => true,
                'supportsNES' => false,
              ),
              'policyFeeTotal' =>
              array (
                'premium' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'minimum' => NULL,
                'premiumAnnualised' => NULL,
                'premiumInsideSuperAnnualised' => NULL,
                'premiumOutsideSuperAnnualised' => NULL,
                'stampDutyAnnualised' => NULL,
                'stampDutyInsideSuperAnnualised' => NULL,
                'stampDutyOutsideSuperAnnualised' => NULL,
                'commissionUpfrontAnnualised' => NULL,
                'commissionOngoingAnnualised' => NULL,
                'premiumOutsideSuper' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'premiumInsideSuper' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'stampDuty' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'stampDutyInsideSuper' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'stampDutyOutsideSuper' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'commissionUpfront' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'commissionOngoing' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'commissionUpfrontPercent' => NULL,
                'commissionOngoingPercent' => NULL,
                'description' => 'Total Policy Fee',
                'breakdown' => NULL,
              ),
              'premiumTotal' =>
              array (
                'premium' =>
                array (
                  'Y' => 86.18,
                  'H' => 46.53,
                  'Q' => 23.270000000000003,
                  'M' => 7.760000000000001,
                  'F' => 3.58,
                  'W' => 1.79,
                ),
                'minimum' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'premiumAnnualised' => NULL,
                'premiumInsideSuperAnnualised' => NULL,
                'premiumOutsideSuperAnnualised' => NULL,
                'stampDutyAnnualised' => NULL,
                'stampDutyInsideSuperAnnualised' => NULL,
                'stampDutyOutsideSuperAnnualised' => NULL,
                'commissionUpfrontAnnualised' => NULL,
                'commissionOngoingAnnualised' => NULL,
                'premiumOutsideSuper' =>
                array (
                  'Y' => 86.18,
                  'H' => 46.53,
                  'Q' => 23.270000000000003,
                  'M' => 7.760000000000001,
                  'F' => 3.58,
                  'W' => 1.79,
                ),
                'premiumInsideSuper' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'stampDuty' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'stampDutyInsideSuper' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'stampDutyOutsideSuper' =>
                array (
                  'Y' => 0.0,
                  'H' => 0.0,
                  'Q' => 0.0,
                  'M' => 0.0,
                  'F' => 0.0,
                  'W' => 0.0,
                ),
                'commissionUpfront' =>
                array (
                  'Y' => 75.83,
                  'H' => 37.92,
                  'Q' => 18.96,
                  'M' => 6.319999999999999,
                  'F' => 2.91,
                  'W' => 1.4500000000000002,
                ),
                'commissionOngoing' =>
                array (
                  'Y' => 18.96,
                  'H' => 9.48,
                  'Q' => 4.75,
                  'M' => 1.59,
                  'F' => 0.7300000000000001,
                  'W' => 0.37000000000000005,
                ),
                'commissionUpfrontPercent' =>
                array (
                  'Y' => 88.0,
                  'H' => 88.0,
                  'Q' => 88.0,
                  'M' => 88.0,
                  'F' => 88.0,
                  'W' => 88.0,
                ),
                'commissionOngoingPercent' =>
                array (
                  'Y' => 22.0,
                  'H' => 22.0,
                  'Q' => 22.0,
                  'M' => 22.0,
                  'F' => 22.0,
                  'W' => 22.0,
                ),
                'description' => 'Total Premium',
                'breakdown' =>
                array (
                  0 =>
                  array (
                    'code' => 'CLE1',
                    'premium' =>
                    array (
                      'Y' => 37.89,
                      'H' => 20.46,
                      'Q' => 10.23,
                      'M' => 3.41,
                      'F' => 1.57,
                      'W' => 0.79,
                    ),
                    'minimum' => NULL,
                    'premiumAnnualised' => NULL,
                    'premiumInsideSuperAnnualised' => NULL,
                    'premiumOutsideSuperAnnualised' => NULL,
                    'stampDutyAnnualised' => NULL,
                    'stampDutyInsideSuperAnnualised' => NULL,
                    'stampDutyOutsideSuperAnnualised' => NULL,
                    'commissionUpfrontAnnualised' => NULL,
                    'commissionOngoingAnnualised' => NULL,
                    'premiumOutsideSuper' =>
                    array (
                      'Y' => 37.89,
                      'H' => 20.46,
                      'Q' => 10.23,
                      'M' => 3.41,
                      'F' => 1.57,
                      'W' => 0.79,
                    ),
                    'premiumInsideSuper' =>
                    array (
                      'Y' => 0.0,
                      'H' => 0.0,
                      'Q' => 0.0,
                      'M' => 0.0,
                      'F' => 0.0,
                      'W' => 0.0,
                    ),
                    'stampDuty' =>
                    array (
                      'Y' => 0.0,
                      'H' => 0.0,
                      'Q' => 0.0,
                      'M' => 0.0,
                      'F' => 0.0,
                      'W' => 0.0,
                    ),
                    'stampDutyInsideSuper' =>
                    array (
                      'Y' => 0.0,
                      'H' => 0.0,
                      'Q' => 0.0,
                      'M' => 0.0,
                      'F' => 0.0,
                      'W' => 0.0,
                    ),
                    'stampDutyOutsideSuper' =>
                    array (
                      'Y' => 0.0,
                      'H' => 0.0,
                      'Q' => 0.0,
                      'M' => 0.0,
                      'F' => 0.0,
                      'W' => 0.0,
                    ),
                    'commissionUpfront' =>
                    array (
                      'Y' => 33.34,
                      'H' => 16.67,
                      'Q' => 8.34,
                      'M' => 2.78,
                      'F' => 1.28,
                      'W' => 0.64,
                    ),
                    'commissionOngoing' =>
                    array (
                      'Y' => 8.34,
                      'H' => 4.17,
                      'Q' => 2.09,
                      'M' => 0.7000000000000001,
                      'F' => 0.32,
                      'W' => 0.16,
                    ),
                    'commissionUpfrontPercent' => NULL,
                    'commissionOngoingPercent' => NULL,
                    'description' => 'Life Cover',
                    'breakdown' =>
                    array (
                      0 =>
                      array (
                        'code' => 'TRM',
                        'premium' =>
                        array (
                          'Y' => 37.89,
                          'H' => 20.46,
                          'Q' => 10.23,
                          'M' => 3.41,
                          'F' => 1.57,
                          'W' => 0.79,
                        ),
                        'minimum' => NULL,
                        'premiumAnnualised' => NULL,
                        'premiumInsideSuperAnnualised' => NULL,
                        'premiumOutsideSuperAnnualised' => NULL,
                        'stampDutyAnnualised' => NULL,
                        'stampDutyInsideSuperAnnualised' => NULL,
                        'stampDutyOutsideSuperAnnualised' => NULL,
                        'commissionUpfrontAnnualised' => NULL,
                        'commissionOngoingAnnualised' => NULL,
                        'premiumOutsideSuper' =>
                        array (
                          'Y' => 37.89,
                          'H' => 20.46,
                          'Q' => 10.23,
                          'M' => 3.41,
                          'F' => 1.57,
                          'W' => 0.79,
                        ),
                        'premiumInsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDuty' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDutyInsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDutyOutsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'commissionUpfront' =>
                        array (
                          'Y' => 33.34,
                          'H' => 16.67,
                          'Q' => 8.34,
                          'M' => 2.78,
                          'F' => 1.28,
                          'W' => 0.64,
                        ),
                        'commissionOngoing' =>
                        array (
                          'Y' => 8.34,
                          'H' => 4.17,
                          'Q' => 2.09,
                          'M' => 0.7000000000000001,
                          'F' => 0.32,
                          'W' => 0.16,
                        ),
                        'commissionUpfrontPercent' => NULL,
                        'commissionOngoingPercent' => NULL,
                        'description' => 'Life',
                        'breakdown' => NULL,
                      ),
                      1 =>
                      array (
                        'code' => 'FEE',
                        'premium' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'minimum' => NULL,
                        'premiumAnnualised' => NULL,
                        'premiumInsideSuperAnnualised' => NULL,
                        'premiumOutsideSuperAnnualised' => NULL,
                        'stampDutyAnnualised' => NULL,
                        'stampDutyInsideSuperAnnualised' => NULL,
                        'stampDutyOutsideSuperAnnualised' => NULL,
                        'commissionUpfrontAnnualised' => NULL,
                        'commissionOngoingAnnualised' => NULL,
                        'premiumOutsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'premiumInsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDuty' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDutyInsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDutyOutsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'commissionUpfront' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'commissionOngoing' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'commissionUpfrontPercent' => NULL,
                        'commissionOngoingPercent' => NULL,
                        'description' => 'Policy Fee',
                        'breakdown' => NULL,
                      ),
                    ),
                  ),
                  1 =>
                  array (
                    'code' => 'CLET',
                    'premium' =>
                    array (
                      'Y' => 48.29,
                      'H' => 26.07,
                      'Q' => 13.04,
                      'M' => 4.35,
                      'F' => 2.0100000000000002,
                      'W' => 1.0,
                    ),
                    'minimum' => NULL,
                    'premiumAnnualised' => NULL,
                    'premiumInsideSuperAnnualised' => NULL,
                    'premiumOutsideSuperAnnualised' => NULL,
                    'stampDutyAnnualised' => NULL,
                    'stampDutyInsideSuperAnnualised' => NULL,
                    'stampDutyOutsideSuperAnnualised' => NULL,
                    'commissionUpfrontAnnualised' => NULL,
                    'commissionOngoingAnnualised' => NULL,
                    'premiumOutsideSuper' =>
                    array (
                      'Y' => 48.29,
                      'H' => 26.07,
                      'Q' => 13.04,
                      'M' => 4.35,
                      'F' => 2.0100000000000002,
                      'W' => 1.0,
                    ),
                    'premiumInsideSuper' =>
                    array (
                      'Y' => 0.0,
                      'H' => 0.0,
                      'Q' => 0.0,
                      'M' => 0.0,
                      'F' => 0.0,
                      'W' => 0.0,
                    ),
                    'stampDuty' =>
                    array (
                      'Y' => 0.0,
                      'H' => 0.0,
                      'Q' => 0.0,
                      'M' => 0.0,
                      'F' => 0.0,
                      'W' => 0.0,
                    ),
                    'stampDutyInsideSuper' =>
                    array (
                      'Y' => 0.0,
                      'H' => 0.0,
                      'Q' => 0.0,
                      'M' => 0.0,
                      'F' => 0.0,
                      'W' => 0.0,
                    ),
                    'stampDutyOutsideSuper' =>
                    array (
                      'Y' => 0.0,
                      'H' => 0.0,
                      'Q' => 0.0,
                      'M' => 0.0,
                      'F' => 0.0,
                      'W' => 0.0,
                    ),
                    'commissionUpfront' =>
                    array (
                      'Y' => 42.49,
                      'H' => 21.25,
                      'Q' => 10.620000000000001,
                      'M' => 3.54,
                      'F' => 1.6300000000000001,
                      'W' => 0.81,
                    ),
                    'commissionOngoing' =>
                    array (
                      'Y' => 10.620000000000001,
                      'H' => 5.3100000000000005,
                      'Q' => 2.66,
                      'M' => 0.8899999999999999,
                      'F' => 0.41000000000000003,
                      'W' => 0.21000000000000002,
                    ),
                    'commissionUpfrontPercent' => NULL,
                    'commissionOngoingPercent' => NULL,
                    'description' => 'TPD Cover',
                    'breakdown' =>
                    array (
                      0 =>
                      array (
                        'code' => 'TPD',
                        'premium' =>
                        array (
                          'Y' => 48.29,
                          'H' => 26.07,
                          'Q' => 13.04,
                          'M' => 4.35,
                          'F' => 2.0100000000000002,
                          'W' => 1.0,
                        ),
                        'minimum' => NULL,
                        'premiumAnnualised' => NULL,
                        'premiumInsideSuperAnnualised' => NULL,
                        'premiumOutsideSuperAnnualised' => NULL,
                        'stampDutyAnnualised' => NULL,
                        'stampDutyInsideSuperAnnualised' => NULL,
                        'stampDutyOutsideSuperAnnualised' => NULL,
                        'commissionUpfrontAnnualised' => NULL,
                        'commissionOngoingAnnualised' => NULL,
                        'premiumOutsideSuper' =>
                        array (
                          'Y' => 48.29,
                          'H' => 26.07,
                          'Q' => 13.04,
                          'M' => 4.35,
                          'F' => 2.0100000000000002,
                          'W' => 1.0,
                        ),
                        'premiumInsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDuty' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDutyInsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'stampDutyOutsideSuper' =>
                        array (
                          'Y' => 0.0,
                          'H' => 0.0,
                          'Q' => 0.0,
                          'M' => 0.0,
                          'F' => 0.0,
                          'W' => 0.0,
                        ),
                        'commissionUpfront' =>
                        array (
                          'Y' => 42.49,
                          'H' => 21.25,
                          'Q' => 10.620000000000001,
                          'M' => 3.54,
                          'F' => 1.6300000000000001,
                          'W' => 0.81,
                        ),
                        'commissionOngoing' =>
                        array (
                          'Y' => 10.620000000000001,
                          'H' => 5.3100000000000005,
                          'Q' => 2.66,
                          'M' => 0.8899999999999999,
                          'F' => 0.41000000000000003,
                          'W' => 0.21000000000000002,
                        ),
                        'commissionUpfrontPercent' => NULL,
                        'commissionOngoingPercent' => NULL,
                        'description' => 'TPD',
                        'breakdown' => NULL,
                      ),
                    ),
                  ),
                ),
              ),
              'premiumTotalProjections' => NULL,
              'score' =>
              array (
                'feature' =>
                array (
                  'raw' => 91.0,
                  15 => 'A',
                  5 => 'A',
                  'star' => '****+',
                ),
                'combined' =>
                array (
                  'raw' => 91.0,
                  15 => 'A',
                  5 => 'A',
                  'star' => '****+',
                ),
                'featurePerNeed' =>
                array (
                  'TRM' =>
                  array (
                    'raw' => 97.14066422941671,
                    15 => 'A+',
                    5 => 'A',
                    'star' => '*****',
                  ),
                  'TPS' =>
                  array (
                    'raw' => 90.67743267048424,
                    15 => 'A',
                    5 => 'A',
                    'star' => '****+',
                  ),
                ),
                'combinedPerNeed' =>
                array (
                  'TRM' =>
                  array (
                    'raw' => 97.0,
                    15 => 'A+',
                    5 => 'A',
                    'star' => '*****',
                  ),
                  'TPS' =>
                  array (
                    'raw' => 91.0,
                    15 => 'A',
                    5 => 'A',
                    'star' => '****+',
                  ),
                ),
              ),
              'errors' =>
              array (
              ),
              'supportsFrequency' =>
              array (
                'Y' => true,
                'H' => false,
                'Q' => false,
                'M' => true,
                'F' => false,
                'W' => false,
              ),
              'allNeedsMet' => true,
              'links' =>
              array (
                'pds' => 'https://www.omnilife.com.au/archive_documents/20161021-Clearview_LifeSolutions.pdf',
                'omniPortal' => false,
                'applicationForm' => true,
                'validation' => false,
                'report' => false,
                'manualPortfolio' => '/CLEP1/manualPortfolioOptions',
                'summaryReport' => NULL,
                'detailedReport' => NULL,
              ),
              'underwriting' =>
              array (
                'limits' =>
                array (
                  'TRM' => 2500000,
                  'TPD' => 2500000,
                ),
                'requirements' =>
                array (
                  0 => 'Personal Statement',
                ),
              ),
              'topFeatures' => NULL,
              'bottomFeatures' => NULL,
              'premiumDiscount' =>
              array (
                'Y' => 0.0,
                'H' => 0.0,
                'Q' => 0.0,
                'M' => 0.0,
                'F' => 0.0,
                'W' => 0.0,
              ),
              'loadings' =>
              array (
                'percentage' => NULL,
                'dollarsPerThousand' => NULL,
              ),
            ),
          ),
        ),
      ),
    ),
    'reports' => NULL,
  ),
);