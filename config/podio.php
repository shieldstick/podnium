<?php

return [
    'keys' => [

        // System wide keys
        'system'    =>  [
            'client_id'     =>  env('PODIO_CLIENT_ID'),
            'client_secret' =>  env('PODIO_CLIENT_SECRET')
        ],

        // Jarickson specific keys
        'jarickson' => [
            'app' => [
                'recommendations' => [
                    'id'    =>  16902755,
                    'token' =>  '7eb3d70a9ff1440a9af1d54ae2fae6f3',
                ]
            ]
        ],

        // Moore Insurance specific keys
        'moore' => [
            'app' => [
                'recommendations' => [
                    'id'    =>  19443393,
                    'token' =>  '771f72baf87a45b7bf52444af1fd6951',
                ]
            ]
        ],

        // East Coast Consultatns specific keys
        'east-coast' => [
            'app' => [
                'recommendations' => [
                    'id'    =>  19402085,
                    'token' =>  '02f96151b98b4496a06d8dc01a384340',
                ]
            ]
        ],

        // Protectinsure specific keys
        'protectinsure' => [
            'app' => [
                'recommendations' => [
                    'id'    =>  19256537,
                    'token' =>  '4fbb6dad61b64c38ab12c5545a0e32f3',
                ]
            ]
        ],

    ],
];