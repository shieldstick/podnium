<?php

Route::group(['middleware' => 'header'], function () {

    // http://domain.com/api/jarickson/recommendations
    Route::post('/jarickson/recommendations', 'Api\JaricksonController@store');
    Route::post('/moore-insurance/recommendations', 'Api\MooreInsuranceController@store');
    Route::post('/east-coast/recommendations', 'Api\EastCoastController@store');
    Route::post('/protectinsure/recommendations', 'Api\ProtectinsureController@store');
});
